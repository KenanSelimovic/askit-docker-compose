# Askit

This is a docker-compose project, setup using git submodules. Submodules "askit" and "askit-backend" have their own repos.

After you've cloned this repo, first thing is to navigate to the root of cloned directory and run following two commands:
* git submodule init
* git submodule update

Prior to running the project, **make sure you have latest docker-compose installed**

https://docs.docker.com/compose/install

## Running:
Both commands may require admin privileges. **Production build requires you to pass in env variables.** As this is for demonstration purposes, you can use the same ones used in dev build.
#### Production build (port 80):
docker-compose up -d --build
#### Dev build (port 8080):
docker-compose -f docker-compose-dev.yml up -d --build

## Possible issues:
#### elastic search exits with error code 78:
Elasticsearch requires specific system config. You can refer to their docs: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html#docker-cli-run-prod-mode
