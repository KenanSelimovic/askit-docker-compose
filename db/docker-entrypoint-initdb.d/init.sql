--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.3
-- Dumped by pg_dump version 9.6.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

-- DROP DATABASE IF EXISTS askit;
--
-- Name: askit; Type: DATABASE; Schema: -; Owner: postgres
--

-- CREATE DATABASE askit WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';


-- ALTER DATABASE askit OWNER TO postgres;

\connect askit

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: log_updates(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION log_updates() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        NEW.updated_at := current_timestamp;
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.log_updates() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: answers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE answers (
    post_id integer NOT NULL,
    question_id integer,
    text text NOT NULL,
    user_id integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE answers OWNER TO postgres;

--
-- Name: answers_post_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE answers_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE answers_post_id_seq OWNER TO postgres;

--
-- Name: answers_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE answers_post_id_seq OWNED BY answers.post_id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE posts (
    id integer NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE posts OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE posts_id_seq OWNER TO postgres;

--
-- Name: posts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE posts_id_seq OWNED BY posts.id;


--
-- Name: questions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE questions (
    post_id integer NOT NULL,
    text text NOT NULL,
    user_id integer,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE questions OWNER TO postgres;

--
-- Name: questions_post_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questions_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questions_post_id_seq OWNER TO postgres;

--
-- Name: questions_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questions_post_id_seq OWNED BY questions.post_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying(50) NOT NULL,
    first_name character varying(50),
    last_name character varying(50),
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    password character varying(60) NOT NULL,
    profile_image_url character varying(100),
    is_deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: vote_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE vote_types (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE vote_types OWNER TO postgres;

--
-- Name: vote_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vote_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vote_types_id_seq OWNER TO postgres;

--
-- Name: vote_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE vote_types_id_seq OWNED BY vote_types.id;


--
-- Name: votes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE votes (
    post_id integer NOT NULL,
    user_id integer NOT NULL,
    type smallint NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE votes OWNER TO postgres;

--
-- Name: answers post_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answers ALTER COLUMN post_id SET DEFAULT nextval('answers_post_id_seq'::regclass);


--
-- Name: posts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts ALTER COLUMN id SET DEFAULT nextval('posts_id_seq'::regclass);


--
-- Name: questions post_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions ALTER COLUMN post_id SET DEFAULT nextval('questions_post_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: vote_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vote_types ALTER COLUMN id SET DEFAULT nextval('vote_types_id_seq'::regclass);


--
-- Name: answers_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('answers_post_id_seq', 1, false);


--
-- Name: posts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('posts_id_seq', 815, true);


--
-- Name: questions_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('questions_post_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 2184, true);


--
-- Data for Name: vote_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY vote_types (id, name, created_at, updated_at) FROM stdin;
1	up	2017-07-15 20:35:50.074967+02	2017-07-15 20:35:50.074967+02
2	down	2017-07-15 20:35:50.074967+02	2017-07-15 20:35:50.074967+02
\.


--
-- Name: vote_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('vote_types_id_seq', 2, true);


--
-- Name: answers answers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (post_id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);


--
-- Name: questions questions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (post_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: vote_types vote_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY vote_types
    ADD CONSTRAINT vote_types_pkey PRIMARY KEY (id);


--
-- Name: votes votes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_pkey PRIMARY KEY (post_id, user_id);


--
-- Name: answers_question_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX answers_question_id_index ON answers USING btree (question_id);


--
-- Name: questions_user_id_is_deleted_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX questions_user_id_is_deleted_index ON questions USING btree (user_id, is_deleted);


--
-- Name: users_email_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX users_email_uindex ON users USING btree (email);


--
-- Name: answers tr_answers_last_updated; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_answers_last_updated BEFORE UPDATE ON answers FOR EACH ROW EXECUTE PROCEDURE log_updates();


--
-- Name: posts tr_posts_last_updated; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_posts_last_updated BEFORE UPDATE ON posts FOR EACH ROW EXECUTE PROCEDURE log_updates();


--
-- Name: questions tr_questions_last_updated; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_questions_last_updated BEFORE UPDATE ON questions FOR EACH ROW EXECUTE PROCEDURE log_updates();


--
-- Name: users tr_users_last_updated; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_users_last_updated BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE log_updates();


--
-- Name: vote_types tr_vote_types_last_updated; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_vote_types_last_updated BEFORE UPDATE ON vote_types FOR EACH ROW EXECUTE PROCEDURE log_updates();


--
-- Name: votes tr_votes_last_updated; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER tr_votes_last_updated BEFORE UPDATE ON votes FOR EACH ROW EXECUTE PROCEDURE log_updates();


--
-- Name: answers answers_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_post_id_fkey FOREIGN KEY (post_id) REFERENCES posts(id);


--
-- Name: answers answers_question_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_question_id_fkey FOREIGN KEY (question_id) REFERENCES questions(post_id);


--
-- Name: answers answers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answers
    ADD CONSTRAINT answers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: questions questions_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_post_id_fkey FOREIGN KEY (post_id) REFERENCES posts(id);


--
-- Name: questions questions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: votes votes_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_post_id_fkey FOREIGN KEY (post_id) REFERENCES posts(id);


--
-- Name: votes votes_type_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_type_fkey FOREIGN KEY (type) REFERENCES vote_types(id);


--
-- Name: votes votes_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY votes
    ADD CONSTRAINT votes_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;

-- CREATE ROLE askit WITH LOGIN;
--
-- Name: answers; Type: ACL; Schema: public; Owner: postgres
--

-- GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE answers TO askit;


--
-- Name: answers_post_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE answers_post_id_seq TO askit;


--
-- Name: posts; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE posts TO askit;


--
-- Name: posts_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE posts_id_seq TO askit;


--
-- Name: questions; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE questions TO askit;


--
-- Name: questions_post_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE questions_post_id_seq TO askit;


--
-- Name: users; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE users TO askit;


--
-- Name: users_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE users_id_seq TO askit;


--
-- Name: vote_types; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE vote_types TO askit;


--
-- Name: vote_types_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,USAGE ON SEQUENCE vote_types_id_seq TO askit;


--
-- Name: votes; Type: ACL; Schema: public; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE votes TO askit;


--
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: askit
--

-- ALTER DEFAULT PRIVILEGES FOR ROLE askit IN SCHEMA public REVOKE ALL ON TABLES  FROM askit;
-- ALTER DEFAULT PRIVILEGES FOR ROLE askit IN SCHEMA public GRANT SELECT,INSERT,DELETE,UPDATE ON TABLES  TO askit;


--
-- PostgreSQL database dump complete
--

